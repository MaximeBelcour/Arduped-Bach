#ifndef B_Arduped
#define B_Arduped
#include <Arduino.h>
#include <Servo.h>

class fonctions
{
  public:
  void tournerTete();
  void tournerDroite();
  void pasChasserDroite();
  void pasChasserGauche();
  void robotAvancer();
  void robotReculer();
  void dancePirate();
  void danceRotation(); //reste à coder
  void dance();

  private:
  Servo servoc;
  Servo servohd;
  Servo servohg;
  Servo servobd;
  Servo servobg;
};

#endif
