#include <Arduino.h>
#include <Servo.h>
#include "B_Arduped.h"

void fonctions::tournerTete()
{
  servoc.write(0); 
  delay(200); 
  servoc.write(70); 
  delay(200); 
  servoc.write(120); 
  delay(200);
  servoc.write(90);
  delay(200);
}

void fonctions::tournerDroite()
{
  servobd.write(150);
  delay(200);
  servobg.write(90);
  delay(200);
  servohg.write(10);
  delay(600);
  servohg.write(50);
  delay(200);
  servobd.write(100);
  servobg.write(70);
  servohg.write(50);
  delay(200);
}

void fonctions::pasChasserDroite()
{
  servohd.write(120);
  servohg.write(100);
  delay(300);
  servohd.write(70);
  servohg.write(50);
  delay(300);
}

void fonctions::pasChasserGauche()
{
  servohd.write(20);
  servohg.write(0);
  delay(300);
  servohd.write(70);
  servohg.write(50);
  delay(300);
}

void fonctions::robotAvancer()
{
  servobd.write(150);//lever le pied droit
  delay(150);
  servohg.write(50);//rotation de la jambre gauche
  delay(150);
  servobd.write(100);//origine
  delay(150);
  servohg.write(50);//origine
  delay(150);
  servobg.write(20);//lever du pied gauche
  delay(150),
  servohd.write(20);//rotation de la jambre droite
  delay(150);
  servobg.write(70);//origine
  delay(150);
  servohd.write(70);//origine
  delay(150);
}

void fonctions::robotReculer()
{
  servobd.write(150);//lever le pied droit
  delay(200);
  servohg.write(0);//rotation de la jambre gauche
  delay(200);
  servobd.write(100);//origine
  delay(200);
  servohg.write(50);//origine
  delay(200);
}

void fonctions::dancePirate()
{
  servobd.write(150);//lever des deux pattes vers l'intérieur
  servobg.write(20);
  delay(600);
  servobd.write(100);//retour à l'origine
  servobg.write(70);
  delay(100);
  servobd.write(150);//lever des deux pattes vers l'intérieur
  servobg.write(20);
  delay(600);
  servobd.write(100);//retour à l'origine
  servobg.write(70);
  delay(100);
  servobd.write(150);//lever des deux pattes vers l'intérieur
  servobg.write(20);
  delay(600);
  servobd.write(100);//retour à l'origine
  servobg.write(70);
  delay(100);
}

void fonctions::danceRotation()
{
  //reste à coder
}

void fonctions::dance()
{
  servobd.write(150);//lever des deux pattes vers l'intérieur
  servobg.write(20);
  delay(500);
  servobd.write(100);//retour à l'origine
  servobg.write(70);
  delay(100);
  servobd.write(110);//lever des deux pattes vers l'intérieur
  servobg.write(30);
  delay(200);
  servobd.write(100);//retour à l'origine
  servobg.write(70);
  delay(100);
  servohg.write(20);//tourner les deux pattes vers l'intérieur
  servohd.write(100);
  delay(400);
  servohg.write(50);//retour à l'origine
  servohd.write(70);
  delay(300);
  servohg.write(80);//tourner les pattes vers l'extérieur
  servohd.write(40);
  delay(400);
  servohg.write(50);//retour à l'origine
  servohd.write(70);
  delay(500);
  servohg.write(80);//tourner les deux pattes vers l'exterieur
  servohd.write(40);
  delay(400);
  servobd.write(150);//lever des pattes croisé
  servobg.write(120);
  delay(500);
  servobd.write(0);//lever des pattes croisé inversé
  servobg.write(20);
  delay(900);
  servobd.write(100);//retour à l'origine
  servobg.write(70);
  delay(500);
  servohg.write(50);//retour à l'origine
  servohd.write(70);
  delay(300);
}

