#include <Dhcp.h>
#include <Dns.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <EthernetServer.h>
#include <EthernetUdp.h>


#include <Servo.h> 
const int pingPin = 2; 
Servo servoc;
Servo servohd;
Servo servohg;
Servo servobd;
Servo servobg;
  
void setup() 
{ 
  servoc.attach(4); //pin du servo
  servoc.write(90); //angle de départ des servo

  servohd.attach(6);
  servohd.write(70); 

  servohg.attach(5);
  servohg.write(50); 

  servobd.attach(7);
  servobd.write(100); 

  servobg.attach(3);
  servobg.write(70); 

  Serial.begin(9600);
} 
  
  
void loop() 
{
  
  long duration, inches, cm;

  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);

  pinMode(pingPin, INPUT);
  duration = pulseIn(pingPin, HIGH);

  inches = microsecondsToInches(duration);
  cm = microsecondsToCentimeters(duration);

  Serial.print(inches);
  Serial.print("in, ");
  Serial.print(cm);
  Serial.print("cm");
  Serial.println();

  delay(100);
  
  if(cm<=8)
  {
    for(int i=0; i<=7; i++)
    {
      int numero = 0;
      numero = random(1,9);
      choix(numero);
    }
  }
}

long microsecondsToInches(long microseconds) {
  return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds) {
  return microseconds / 29 / 2;
}

void choix(int maxime)
{
 if (maxime==1)
 {
   tournerTete();
   return;
 }

 if (maxime==2)
 {
   tournerDroite();
   return;
 }

 if (maxime==3)
 {
   pasChasserDroite();
   return;
 }

 if (maxime==4)
 {
   pasChasserGauche();
   return;
 }

 if (maxime==5)
 {
   robotAvancer();
   return;
 }

 if (maxime==6)
 {
   robotReculer();
   return;
 }

 if (maxime==7)
 {
   dancePirate();
   return;
 }

 if (maxime==8)
 {
   danceRotation();
   return;
 }

 if (maxime==9)
 {
   dance();
   return;
 }

 
}

void tournerTete()
{
  servoc.write(0); 
  delay(200); 
  servoc.write(70); 
  delay(200); 
  servoc.write(120); 
  delay(200);
  servoc.write(90);
  delay(200);
}

void tournerDroite()
{
  servobd.write(150);
  delay(200);
  servobg.write(90);
  delay(200);
  servohg.write(10);
  delay(600);
  servohg.write(50);
  delay(200);
  servobd.write(100);
  servobg.write(70);
  servohg.write(50);
  delay(200);
}

void pasChasserDroite()
{
  servohd.write(120);
  servohg.write(100);
  delay(300);
  servohd.write(70);
  servohg.write(50);
  delay(300);
}

void pasChasserGauche()
{
  servohd.write(20);
  servohg.write(0);
  delay(300);
  servohd.write(70);
  servohg.write(50);
  delay(300);
}

void robotAvancer()
{
  servobd.write(150);//lever le pied droit
  delay(150);
  servohg.write(50);//rotation de la jambre gauche
  delay(150);
  servobd.write(100);//origine
  delay(150);
  servohg.write(50);//origine
  delay(150);
  servobg.write(20);//lever du pied gauche
  delay(150),
  servohd.write(20);//rotation de la jambre droite
  delay(150);
  servobg.write(70);//origine
  delay(150);
  servohd.write(70);//origine
  delay(150);
}

void robotReculer()
{
  servobd.write(150);//lever le pied droit
  delay(200);
  servohg.write(0);//rotation de la jambre gauche
  delay(200);
  servobd.write(100);//origine
  delay(200);
  servohg.write(50);//origine
  delay(200);
}

void dancePirate()
{
  //saute 3 fois
  servobd.write(150);//lever des deux pattes vers l'intérieur
  servobg.write(20);
  delay(600);
  servobd.write(100);//retour à l'origine
  servobg.write(70);
  delay(100);
  servobd.write(150);//lever des deux pattes vers l'intérieur
  servobg.write(20);
  delay(600);
  servobd.write(100);//retour à l'origine
  servobg.write(70);
  delay(100);
  servobd.write(150);//lever des deux pattes vers l'intérieur
  servobg.write(20);
  delay(600);
  servobd.write(100);//retour à l'origine
  servobg.write(70);
  delay(100);    
}

void danceRotation()
{
  
}

void dance()
{
  servobd.write(150);//lever des deux pattes vers l'intérieur
  servobg.write(20);
  delay(500);
  servobd.write(100);//retour à l'origine
  servobg.write(70);
  delay(100);
  servobd.write(110);//lever des deux pattes vers l'intérieur
  servobg.write(30);
  delay(200);
  servobd.write(100);//retour à l'origine
  servobg.write(70);
  delay(100);
  servohg.write(20);//tourner les deux pattes vers l'intérieur
  servohd.write(100);
  delay(400);
  servohg.write(50);//retour à l'origine
  servohd.write(70);
  delay(300);
  servohg.write(80);//tourner les pattes vers l'extérieur
  servohd.write(40);
  delay(400);
  servohg.write(50);//retour à l'origine
  servohd.write(70);
  delay(500);
  servohg.write(80);//tourner les deux pattes vers l'exterieur
  servohd.write(40);
  delay(400);
  servobd.write(150);//lever des pattes croisé
  servobg.write(120);
  delay(500);
  servobd.write(0);//lever des pattes croisé inversé
  servobg.write(20);
  delay(900);
  servobd.write(100);//retour à l'origine
  servobg.write(70);
  delay(500);
  servohg.write(50);//retour à l'origine
  servohd.write(70);
  delay(300);
}

